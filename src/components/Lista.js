import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';


export default class Lista extends Component{

    constructor(props){
      super(props);
      this.state = {
        feed: this.props.data
      };

      this.carrrgaIcone = this.carrrgaIcone.bind(this);
      this.like = this.like.bind(this);

    }

    //aqui muda o status de likeada
    like(){
      let state = this.state;

      if(state.feed.likeada == true){
        state.feed.likeada = false;
        state.feed.likers -= 1;
      }else{
        state.feed.likeada = !state.feed.likeada;
        state.feed.likers += 1;
      }

      this.setState(state);
    }

    //Aqui carrega o ícone se for likeada ou não
    carrrgaIcone(likeada){
      return likeada ? require('../img/likeada.png') : require('../img/like.png')
    }

    //Mostra a quantidade de likes na foto
    exibeLike(likers){
      let state = this.state;

      if(state.feed.likers <= 0){
        return;
      }

      return(
        <Text style={styles.like}>
          {state.feed.likers} {state.feed.likers > 1 ? 'curtidas' : 'curtida'}
        </Text>
      );
    }

    render(){
      return(
      <View style={styles.areaFeed}>

        <View style={styles.viewPerfil}>
          <Image  style={styles.fotoPerfil} source={{uri: this.state.feed.imgperfil}}/>
          <Text style={styles.usuarioNome}>{this.state.feed.nome}</Text>
        </View>

        <Image resizeMode='cover' style={styles.fotoPublicada} source={{ uri: this.state.feed.imgPublicacao }}/>

        <View style={styles.areaBotoes}>
          <TouchableOpacity onPress={this.like}>
            <Image style={styles.iconeLike} source={this.carrrgaIcone(this.state.feed.likeada)}/>
          </TouchableOpacity>
           <TouchableOpacity style={{ paddingLeft: 5}} onPress={() => {}}>
            <Image style={styles.iconeLike} source={require('../img/send.png')}/>
          </TouchableOpacity>
        </View>

        {this.exibeLike(this.state.feed.likers)}

        <View style={styles.viewRodape}>
          <Text style={styles.nomeRodape}>
            {this.state.feed.nome}
          </Text>
          <Text style={styles.descRodape}>
            {this.state.feed.descricao}
          </Text>
        </View>

      </View>
      );
    }
}

    


  const styles = StyleSheet.create({
    areaFeed:{
      marginBottom: 10,
    },
    fotoPerfil:{
      width: 70,
      height: 70,
      borderRadius: 50
   },
    viewPerfil:{
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      padding: 8
   },
    usuarioNome:{
      fontSize: 20,
      textAlign: 'left',
      paddingLeft: 8
    },
    fotoPublicada:{
      flex:1,
      height: 400,
      alignItems: "center"
    },
    areaBotoes:{
      flex: 1,
      flexDirection: 'row',
      padding: 5
    },
    iconeLike: {
      width: 33,
      height: 33
    },
    like:{
      color: '#000000',
      fontWeight: 'bold',
      paddingLeft: 8
    },
    viewRodape:{
      flexDirection: 'row',
      alignItems: 'center'
    },
    nomeRodape:{
      fontSize: 18,
      fontWeight: 'bold',
      color: '#000000',
      paddingLeft: 8
    },
    descRodape:{
      fontSize: 15,
      color: '#000000',
      paddingLeft: 8
    }
  });